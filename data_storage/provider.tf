provider "aws" {
    region      = var.region
}

terraform {
    backend "s3" {
        bucket  = "finalcloud-state-files"
        key     = "data.tf"
        region  = "eu-west-3"
    }
}