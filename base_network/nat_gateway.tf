resource "aws_nat_gateway" "nat" {
    subnet_id               = aws_subnet.dmz1.id
    allocation_id           = aws_eip.nat_eip.id
#    network_interface_id    = aws_network_interface.net_interface.id     # Why isn't this working?
    depends_on              = [aws_internet_gateway.igw]
    
    tags= {
        Name = "nat_gateway"
    }
}

resource "aws_network_interface" "net_interface" {
    subnet_id       = aws_subnet.dmz1.id
    security_groups = [aws_security_group.dmz.id]
    
    tags = {
        Name = "network_interface"
    }
}

resource "aws_eip" "nat_eip" {
    depends_on  = [aws_internet_gateway.igw]
    vpc         = true

    tags = {
        Name = "nat_eip"
    }

}