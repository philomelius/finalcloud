resource "aws_vpc" "wordpress" {
  cidr_block  = "10.0.0.0/16"

  tags = {
        Name = "Wordpress"
    }
}

resource "aws_internet_gateway" "igw"{
  vpc_id    = aws_vpc.wordpress.id 

  tags = {
      Name = "WordpressIGW"
  }
}

