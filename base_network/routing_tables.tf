# Routing tables

resource "aws_route_table" "public" {
    vpc_id  = aws_vpc.wordpress.id 

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_internet_gateway.igw.id
    }

    tags = {
        Name = "public_route_table"
    }
} 


resource "aws_route_table" "private" {
    vpc_id      = aws_vpc.wordpress.id
    
    route {
        cidr_block = "0.0.0.0/0"
        gateway_id  = aws_nat_gateway.nat.id
    }

    tags = {
        Name = "private_route_table"
    }
}



# Associations

    # Apps and DB subnets

        # Apps

resource "aws_route_table_association" "az1" {
    subnet_id       = aws_subnet.az1.id
    route_table_id  = aws_route_table.private.id
}


resource "aws_route_table_association" "az2" {
    subnet_id       = aws_subnet.az2.id
    route_table_id  = aws_route_table.private.id
}


resource "aws_route_table_association" "az3" {
    subnet_id       = aws_subnet.az3.id
    route_table_id  = aws_route_table.private.id
}

        # DB

resource "aws_route_table_association" "dbaz1" {
    subnet_id       = aws_subnet.az1.id
    route_table_id  = aws_route_table.private.id
}


resource "aws_route_table_association" "dbaz2" {
    subnet_id       = aws_subnet.az2.id
    route_table_id  = aws_route_table.private.id
}


resource "aws_route_table_association" "dbaz3" {
    subnet_id       = aws_subnet.az3.id
    route_table_id  = aws_route_table.private.id
}

    # DMZ

resource "aws_route_table_association" "dmz1" {
    subnet_id       = aws_subnet.dmz1.id
    route_table_id  = aws_route_table.public.id
}

resource "aws_route_table_association" "dmz2" {
    subnet_id       = aws_subnet.dmz2.id
    route_table_id  = aws_route_table.public.id
}

resource "aws_route_table_association" "dmz3" {
    subnet_id       = aws_subnet.dmz3.id
    route_table_id  = aws_route_table.public.id
}