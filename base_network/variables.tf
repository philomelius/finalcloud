
variable "region" {
    type = string
    default = "eu-west-3"
}

variable "zone" {
    type = list
    default = ["a", "b", "c"]
}

variable "server_port" {
    default = "80"
}


locals {
    bucket_name = "finalcloud-state-files"
}

locals {
    dmz_subnets = [aws_subnet.dmz1.id, aws_subnet.dmz2.id, aws_subnet.dmz3.id]
}

locals {
    apps_subnets = [aws_subnet.az1.id,aws_subnet.az2.id,aws_subnet.az3.id]
}

locals {
    db_subnets = [aws_subnet.dbaz1.id,aws_subnet.dbaz2.id,aws_subnet.dbaz3.id]
}

locals {
    dmz_cidr = [aws_subnet.dmz1.cidr_block, aws_subnet.dmz2.cidr_block, aws_subnet.dmz3.cidr_block]
}

locals {
    apps_cidr = [aws_subnet.az1.cidr_block, aws_subnet.az2.cidr_block, aws_subnet.az3.cidr_block]
}

locals {
    db_cidr = [aws_subnet.dbaz1.cidr_block, aws_subnet.dbaz2.cidr_block, aws_subnet.dbaz3.cidr_block]
}


# Key name in AWS Secrets Manager
#variable "db_secret" {
 #   default = "db-creds"
#}


