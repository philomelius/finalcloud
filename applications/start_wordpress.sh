#!/bin/bash

# Primary setup

sudo yum install epel-release -y
sudo yum install ansible -y
sudo yum install wget -y
wget https://gitlab.com/philomelius/finalcloud/-/raw/master/ansible/wordpress.yml
wget https://gitlab.com/philomelius/finalcloud/-/raw/master/ansible/wp_config.yml
ansible-galaxy collection install community.aws
echo "localhost" >> inv

# Install Ansible packages and setup RDS

ansible-playbook wordpress.yml -i inv --connection local

# Deploy Wordpress

ansible-playbook wp_config.yml -i inv --connection local


