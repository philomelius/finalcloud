# Applications outputs

output "key_name" {
    value   = var.key_name
}

output "region" {
    value   = var.region
}

output "instance_type" {
    value   = var.instance_type
}

output "image_id" {
    value   = var.image_id
}

#output "bastion_ip" {
#    value   = aws_autoscaling_group.bastion.public_ip
#}
