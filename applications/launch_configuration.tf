# Launch configuration

resource "aws_launch_configuration" "main" {
    instance_type               = var.instance_type
    image_id                    = var.image_id
    key_name                    = var.key_name
    security_groups             = [data.terraform_remote_state.networking.outputs.security_group_apps]
    user_data                   = file("start_wordpress.sh")
    associate_public_ip_address = false

    lifecycle {
        create_before_destroy = true
    }
}

resource "aws_launch_configuration" "dmz"{
    instance_type               = var.bastion_type
    image_id                    = var.image_id
    key_name                    = var.key_name
    security_groups             = [data.terraform_remote_state.networking.outputs.security_group_dmz]
    user_data                   = file("start_wordpress.sh")
    associate_public_ip_address = true

    lifecycle {
        create_before_destroy = true
    }
}