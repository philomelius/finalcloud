variable "region" {
    type = string
    default = "eu-west-3"
}

variable "bucket_name" {
    default = "finalcloud-state-files"
}