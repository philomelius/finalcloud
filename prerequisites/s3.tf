
# S3 bucket for state files

resource "aws_s3_bucket" "state" {
    bucket  = var.bucket_name
    acl     = "private"
  # force_destroy = true

    versioning {
        enabled = true
    }

    server_side_encryption_configuration {

        rule {
        apply_server_side_encryption_by_default {
            sse_algorithm = "AES256"
            }
       }
    }
}


# DynamoDB for state file management

resource "aws_dynamodb_table" "dynamo" {
    name            = "dynamo"
    billing_mode    = "PAY_PER_REQUEST"
    hash_key        = "LockID"

    attribute {
        name = "LockID"
        type = "S"
    }
}